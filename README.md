## React Php Mysql with Docker starterkit

### Clone this project
```console
git clone https://zaytseff@bitbucket.org/zaytseff/rpmd.git
```
### Start docker

```console
cd ./rpmd
docker-compose up --build -d
```
### Fix MySQL LOGIN
Enter into DB container:
```console
docker-compose exec db bash
mysql -u root -proot

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'root';
quit;
```
exit from container

### Install symfony/skeleton

Enter into php container
```console
docker-compose exec php bash
cd /var/www/api
composer install
...install process
exit
```
open http://localhost in browser

### Install react boilerplate
```console
cd ./ui
npm install
...install process
npm start
```
open http://localhost:3000 in browser
